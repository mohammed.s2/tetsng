package AssignmentsTestNG;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AddUserTC {
	WebDriver driver;
	@Test
	public void addUser() throws Exception {
		Actions act = new Actions(driver);
		driver.findElement(By.xpath("//span[text()='Admin']")).click();
	    driver.findElement(By.xpath("//div[@class='orangehrm-header-container']/button")).click();
	    List<WebElement> list = driver.findElements(By.xpath("//div[@class='oxd-select-text-input']"));
	    list.get(0).click();
	    act.sendKeys(Keys.ARROW_DOWN).build().perform();
	    Thread.sleep(3000);
	    list.get(0).click();
	    list.get(1).click();
	    act.sendKeys(Keys.ARROW_DOWN).build().perform();
	    Thread.sleep(3000);
	    list.get(0).click();
	    driver.findElement(By.xpath("//input[@placeholder = 'Type for hints...']")).sendKeys("Dominic  Chase");
	    act.sendKeys(Keys.ARROW_DOWN);
	    Thread.sleep(3000);
	    act.sendKeys(Keys.ENTER).build().perform();
	    List<WebElement> inputList = driver.findElements(By.className("oxd-input oxd-input--active"));
	    inputList.get(1).sendKeys("ABCDs");
	    inputList.get(2).sendKeys("1234..Hart");
	    inputList.get(3).sendKeys("1234..Hart");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	}
	@BeforeClass
	public void login() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
		driver.findElement(By.name("username")).sendKeys("Admin");
		driver.findElement(By.name("password")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[text()=' Login ']")).click();
	}
	@AfterClass
	public void quit() {
        driver.quit();
	}
}
